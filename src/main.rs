// External crates
extern crate json;
extern crate tokio;
extern crate termion;
extern crate reqwest;
extern crate encoding;

// Imports
use termion::input::TermRead;
use termion::{color, style};
use termion::raw::IntoRawMode;
use encoding::{DecoderTrap, Encoding, EncoderTrap};
use encoding::all::ISO_8859_1;

// Standard imports
use std::io::{Write, stdout, stdin};
use std::process::Command;
use std::ffi::OsString;

struct YoutubeVideo {
    title: String,
    video_id: String,
    channel_id: String,
    description: String,
 }
impl YoutubeVideo {
    fn new(title: String,
        video_id: String,
        channel_id: String,
        description: String) -> YoutubeVideo {
        YoutubeVideo{title: title, video_id: video_id, channel_id: channel_id, description: description}
    }
}
// Fill entire screen with stuff
fn fill_space(left_string: &str, right_string: &str, width: u16) -> String {
    // TODO
    let mut result: String = String::new();
    let mut space_to_fill: i32 = 0;
    let mut new_left_string: String = left_string.clone().to_string();
    
    // Removes unicode characters as they are difficult to truncate. Would be nice to fix this
    new_left_string = ISO_8859_1.decode(&ISO_8859_1.encode(&new_left_string, EncoderTrap::Ignore).unwrap(), DecoderTrap::Ignore).unwrap();
    
    if new_left_string.chars().count() as i32 >= (width as i32 - right_string.chars().count() as i32) {
        if new_left_string.chars().count() as u16 >= width as u16 - (right_string.chars().count() as u16 + 1) {
            new_left_string = truncate_string(&new_left_string, width as u16 - (right_string.chars().count() as u16 + 1), true);     
        }
    }
    space_to_fill = width as i32 - (new_left_string.chars().count() as i32 + right_string.chars().count() as i32);
    if space_to_fill > 0 {
        for _x in 0..space_to_fill {
            result += " ";
        }
    }
    result = format!("{}{}{}", new_left_string, result, right_string);
    return result;
}

// Creates string from video vector
fn display_videos(video_vector: &Vec<YoutubeVideo>, dimensions: (u16, u16)) -> String {
    // TODO
    let mut output: String = String::new();
    let mut _is_even: bool = false;
    output += &format!("{}", style::Underline); 
    for x in video_vector {
        if _is_even {
            output += &format!("{}{}\n", color::Fg(color::Yellow), fill_space(&x.title, "00:00", dimensions.0)); 
        } else { 
            output += &format!("{}{}\n", color::Fg(color::LightYellow), fill_space(&x.title, "00:00", dimensions.0)); 
        }
        // Toggling
        _is_even = !_is_even; 
    }
    output
}

async fn do_search(search_term: String, sort_by: String, api_key: String, dimensions: (u16, u16), search_type: String) -> String {
    // TODO
    // Epic closure, love this kind of clusterfuck, very compact
    let _limit = | i: u16 | -> u16 { if i >= 50 { return 50 as u16} return i; };
    let mut search_uri: String = "https://www.googleapis.com/youtube/v3/search?part=snippet".to_string();
    search_uri += &format!("&q={}", search_term);
    search_uri += &format!("&key={}", api_key);
    search_uri += &format!("&maxResults={}", _limit(dimensions.1)-1);
    search_uri += &format!("&type={}", search_type);
    let body = reqwest::get(&search_uri).await.unwrap().text().await.unwrap();
    return body;
}

fn full_clear() {
    let output = Command::new("clear").output().unwrap();
    println!("{}", String::from_utf8_lossy(&output.stdout));
}

fn screen_clear() {
    print!("{}{}", termion::cursor::Goto(1, 1), termion::clear::AfterCursor);
}

fn reset_style() {
    print!("{}{}", termion::style::Reset, termion::color::Fg(termion::color::Reset));
}

fn goto_bottom(rows: u16) {
    print!("{}", termion::cursor::Goto(1, rows));
}

fn get_video_vec(json_data: json::JsonValue) -> Vec<YoutubeVideo> {
    if !json_data["error"].is_null() {
        for err in json_data["error"]["errors"].members() {
            println!("{}ERROR:{} {}",color::Fg(color::LightRed), color::Fg(color::Reset), err["message"].to_string());
        }
    }
    let mut result: Vec<YoutubeVideo> = Vec::new();
    for _x in json_data["items"].members() {
        result.push(YoutubeVideo::new(_x["snippet"]["title"].to_string(),
        _x["id"]["videoId"].to_string(),
        _x["snippet"]["channelId"].to_string(),
        _x["snippet"]["description"].to_string()));
            
    }
    result
}

fn truncate_string(input: &str, length: u16, dots: bool) -> String {
    // Cloning input
    let mut cloned_input: String = input.clone().to_string();
    let mut result: String = ISO_8859_1.decode(&ISO_8859_1.encode(&cloned_input, EncoderTrap::Ignore).unwrap(), DecoderTrap::Ignore).unwrap();
    
    if dots && length > 3 {  
        result.truncate((length-3) as usize);
        result = format!("{}...", result);
    } else {
        result.truncate(length as usize);
    }
    
    // Returning
    result
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Set up variables 
    let mut input: String = String::new();
    let stdin = stdin();
    let mut stdin = stdin.lock();
    let stdout = stdout();
    let mut stdout = stdout.lock();
    let mut terminal_size = termion::terminal_size().unwrap(); 
    let mut video_vector: Vec<YoutubeVideo> = Vec::new();
    let mut _json_result: json::JsonValue = json::parse(r#"{}"#).unwrap();
    let mut _search_body: String = String::new();
    // Add these to config file later
    let _player = "mpv";
    let _api_key = "AIzaSyCLckan82QtFawMbG_YwcFgLoklOYUFfio";
    let _region = "US";
    screen_clear();
    goto_bottom(terminal_size.1);
    stdout.flush().unwrap();
    // Main event loop, quits if message starts with quit
    while !input.starts_with("quit") {    
        // Reset input
        input = "".to_string();
        
        // Get input 
        input = stdin.read_line().unwrap().unwrap();
        
        // Get terminal size so you can resize the window
        terminal_size = termion::terminal_size().unwrap();
         
        // Process input, doing this with a match statement might be possible.
        if input.starts_with("/") {
            // Search videos
            print!("{}{}", termion::cursor::Goto(1, 1), termion::clear::AfterCursor);
            // Perform search, based on terminal size
            _search_body = do_search(input.replacen("/", "", 1), "relevance".to_string(), _api_key.to_string(), terminal_size, "video".to_string()).await;
            // Parse json and get video vector
            _json_result = json::parse(&_search_body).expect("Unable to unwrap");
            video_vector = get_video_vec(_json_result); 
            // Print videos
            print!("{}", display_videos(&video_vector, terminal_size));
            goto_bottom(terminal_size.1);
            reset_style();
            stdout.flush().unwrap();
        }
        else if input.starts_with("?") {
            // Search channels
        }
        else if input.starts_with("!") {
            // Set settings
        }
        else if input.starts_with("n") {
            // Go to next page
        }
        else if input.starts_with("p") {
            // Go to previous page
        }
        else {
            // Redraw vector
            // Play video
            print!("{}", display_videos(&video_vector, terminal_size));
            goto_bottom(terminal_size.1);
            reset_style();
            stdout.flush().unwrap();
        }
    }
    screen_clear();
    println!("{}{}Thank you for using Rythm!", termion::clear::All, termion::cursor::Goto(1,1));
    Ok(())
}

