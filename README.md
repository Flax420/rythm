# RYTHM
RYTHM a simple mps-youtube alternative written in Rust.

# Why
The idea for Rythm is to be faster and simpler. Ultimate goal is to have every good feature from mps-youtube but none of the bloat and proper support for any resolution.